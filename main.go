package main

import (
	"fmt"
	"log"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

var (
	defaults = map[string]interface{}{
		"username": "admin",
		"password": "password1",
		"host":     "localhost",
		"port":     3306,
		"database": "test",
	}
	configName  = "config"
	configPaths = []string{
		".",
	}
)

type Config struct {
	Username string
	Password string
	Host     string
	Port     int
	Database string
}

func main() {
	for k, v := range defaults {
		viper.SetDefault(k, v)
	}
	viper.SetConfigName(configName)
	for _, p := range configPaths {
		viper.AddConfigPath(p)
	}
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("could not read config file: %v", err)
	}
	fmt.Printf("Username from viper: %s\n", viper.GetString("username"))
	fmt.Printf("Password from viper: %s\n", viper.GetString("password"))
	fmt.Printf("Host from viper: %s\n", viper.GetString("host"))
	fmt.Printf("Port from viper: %d\n", viper.GetInt("port"))
	fmt.Printf("Database from viper: %s\n", viper.GetString("database"))

	var config Config
	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("could not decode config into struct: %v", err)
	}
	fmt.Printf("Username from struct: %s\n", config.Username)
	fmt.Printf("Config struct: %v\n", config)
	changed := false
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		err = viper.Unmarshal(&config)
		if err != nil {
			log.Printf("could not decode config after changed: %v", err)
		}
		changed = true
	})
	for !changed {
		time.Sleep(time.Second)
		fmt.Printf("Config struct: %v\n", config)
	}
}
